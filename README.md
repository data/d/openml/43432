# OpenML dataset: Chess-Position--Chess-Moves

https://www.openml.org/d/43432

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
The objective of this dataset is to create a chess engine through machine learning. In this first part we will first predict the pieces to be moved depending on the position of the chessboard
This is inspired by this research (https://pdfs.semanticscholar.org/28a9/fff7208256de548c273e96487d750137c31d.pdf) but by comparing several approaches and having the best performance
The data used by this competition is a processed version of the dataset https://www.kaggle.com/milesh1/35-million-chess-games which
Content
The players are represented by the winning player and the losing player.
The first 64 columns represent the 64 squares of the chessboard and each value corresponds to the piece which is on this square
the unique values are as follows

WR: Rook of wining player
WB: Bishop of wining player
WN: Knight of wining player
WQ: Queen of wining player
WK: King of wining player
WP: Pawn of wining player
LR : Rook of losing player
LB : Bishop of losing player
LN : Knight of losing player
LQ : Queen of losing player
LK : King of losing player
LP : Pawn of losing player

The next turn is the winner's turn so the output is the algebrical value of the box containing the piece to move A1 to H8 (64 classes)

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43432) of an [OpenML dataset](https://www.openml.org/d/43432). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43432/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43432/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43432/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

